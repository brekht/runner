<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'sqlite:'.realpath(__DIR__."/../database")."/db.sqlite",
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];
