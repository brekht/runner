<?php

use app\models\User;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

if ($model->isNewRecord) {
    $this->title = "Новый пользователь";
} else {
    $this->title = $model->username;
}
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
    <?php
        if ($model->isNewRecord) {
            echo $form->field($model, 'password')->passwordInput(['maxlength' => true]);
        }
    ?>
    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
    <?php
        echo $form->field($model, 'role')->dropDownList([
            User::ROLE_MANAGER => User::getRoleTitle(User::ROLE_MANAGER),
            User::ROLE_ADMIN => User::getRoleTitle(User::ROLE_ADMIN)
        ]);
    ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>