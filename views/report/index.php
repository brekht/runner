<?php

use app\models\Kitchen;
use app\models\Runner;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use nex\datepicker\DatePicker;
use yii\grid\GridView;
use app\models\Route;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $report app\models\MainReport */

$this->title = 'Отчёт за период';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="runner-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin([
        'options' => ['data-pjax' => true],
        'method' => 'GET'
    ]) ?>

    <div class="row">
        <div class="col-md-6">
            <?php
                echo $form->field($report, 'start_date')->widget(
                    DatePicker::className(), [
                    'language' => 'ru',
                    'size' => 'sm',
                    'clientOptions' => [
                        'format' => 'L LT',
                        'stepping' => 30,
                    ],
                ]);
            ?>
        </div>
        <div class="col-md-6">
            <?php
                echo $form->field($report, 'end_date')->widget(
                    DatePicker::className(), [
                    'language' => 'ru',
                    'size' => 'sm',
                    'clientOptions' => [
                        'format' => 'L LT',
                        'stepping' => 30,
                    ],
                ]);
            ?>
        </div>
        <div class="col-md-12">
            <?php
            echo $form->field($report, 'start_point')->dropDownList(Kitchen::getAddresses());
            ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Показать', [
            'class' => 'btn btn-primary',
            'name' => "MainReport[action]",
            'value' => 'show'
        ]) ?>
        <?= Html::submitButton('Скачать', [
            'class' => 'btn btn-default pull-right',
            'name' => "MainReport[action]",
            'value' => 'download',
            'data-pjax' => 0
        ]) ?>
    </div>

    <?php ActiveForm::end() ?>

    <?php
    Pjax::begin([
        'id' => 'container-id',
        'timeout' => false,
        //'enablePushState' => false,
        'clientOptions' => ['method' => 'GET']])
    ?>

    <?php
        if (!empty($report->start_date) && !empty($report->end_date)) {
            $mainColumns = [
                [
                    'header' => "Курьер",
                    'format' => "raw",
                    'value' => function($report) {
                        $time = \Yii::$app->formatter->asTime($report['created'], "dd.MM.yyyy HH:mm");
                        if ($report['runner_last_name']=='') {
                            return "
                                <p style='text-align: right; padding: 0;'><sup>$time</sup></p>
                                <span style='color:red'>Курьер удалён</span> id:{$report['runner_id']}
                            ";
                        }
                        $phone = isset($report['phone'])?"<br /><i>{$report['phone']}</i>":'';
                        return "
                            <p style=\"text-align: right; padding: 0;\"><sup>{$time}</sup></p>
                            {$report['runner_last_name']} {$report['runner_first_name']}
                            $phone
                        ";
                    }
                ],
                [
                    'header' => "Точки",
                    'format' => "raw",
                    'value' => function($report) {
                        $result = '';
                        $route = Route::findOne($report['route_id']);
                        foreach ($route->points as $point) {
                            $result .= "<li>{$point->address}</li>";
                        }
                        return "<ol>$result</ol>";
                    }
                ],
                [
                    'header' => "Дистанция(км)",
                    'format' => "raw",
                    'value' => function($report) {
                        if ($report['bonus']) {
                            return \Yii::$app->formatter->asDecimal(($report['distance']) / 1000, 3) .
                                " + " .
                                "<span style='color:green'>" .
                                \Yii::$app->formatter->asDecimal(($report['bonus']) / 1000, 3) .
                                "</span>" .
                                "<hr />" .
                                "<strong>" .
                                \Yii::$app->formatter->asDecimal(($report['distance'] + $report['bonus']) / 1000, 3) .
                                "</strong>";
                        } else {
                            return \Yii::$app->formatter->asDecimal(($report['distance']) / 1000, 3);
                        }
                    }
                ],
            ];

            $sumColumns = [
                [
                    'header' => "Курьер",
                    'format' => 'raw',
                    'value' => function($sum) {
                        if ($runner = Runner::findOne($sum['runner_id'])) {
                            return $runner->last_name . " " . $runner->first_name;
                        }
                        return '<span style=\'color:red\'>Курьер удалён</span> id:'.$sum['runner_id'];
                    }
                ],
                [
                    'header' => "Дистанция(км)",
                    'format' => 'raw',
                    'value' => function($sum) {
                        if ($sum['bonus']) {
                            return \Yii::$app->formatter->asDecimal(($sum['distance']) / 1000, 3) .
                                " + " .
                                "<span style='color:green'>" .
                                \Yii::$app->formatter->asDecimal(($sum['bonus']) / 1000, 3) .
                                "</span>" .
                                " = " .
                                "<strong>" .
                                \Yii::$app->formatter->asDecimal(($sum['distance'] + $sum['bonus']) / 1000, 3) .
                                "</strong>";
                        } else {
                            return \Yii::$app->formatter->asDecimal(($sum['distance']) / 1000, 3);
                        }
                    }
                ]
            ];


            try {
                echo "<h3>Суммарно за период</h3>";
                echo GridView::widget([
                    'dataProvider' => $report->getSumDataProvider(),
                    'columns' => $sumColumns
                ]);

                echo "<h3>Раздельно за период</h3>";
                echo GridView::widget([
                    'dataProvider' => $report->getMainDataProvider(),
                    'columns' => $mainColumns
                ]);
            } catch (\Exception $e) {
                echo $e->getMessage();
            }
        }
    ?>
    <?php Pjax::end() ?>
</div>
