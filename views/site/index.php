<?php

use yii\helpers\Html;
use app\models\Runner;

/* @var $this yii\web\View */
/* @var $runners Runner[] */

$this->title = 'Курьер';
$this->registerJsFile("https://api-maps.yandex.ru/2.1/?lang=ru_RU");
//$this->registerJsFile("js/site.js", ['position'=>\yii\web\View::POS_END]);
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <div class="col-lg-8">
                <h2>Курьер</h2>
                <button
                    type="button"
                    id="runner"
                    class="btn btn-primary btn-lg btn-block"
                    data-toggle="modal"
                    data-target="#runner-select-modal"
                    data-runner-id="-1"
                >
                    Выбрать курьера
                </button>
                <h2>Маршрут<a class="btn btn-info" style="float:right;" onclick="addKitchenToRoute();">Кухня</a></h2>
                <div id="route">
                    <ol class="list-group"></ol>
                    <div class="input-group">
                        <input type="text" id="point-search" class="form-control" placeholder="введите адрес"/>
                        <span class="input-group-btn">
                            <button id="add-that-point" class="btn btn-default" type="button" onclick="addThatPoint(this)" disabled>Добавить</button>
                        </span>
                    </div>
                    <button type="button" id="calculate-route" class="btn btn-primary btn-lg btn-block" onclick="calculate();">Посчитать</button>
                </div>
                <div id="route-length" class="alert alert-info" role="alert" style="display: none;">
                    <div class="badge"></div>
                    <button type="button" id="assign-route" class="btn btn-primary btn-lg btn-block" onclick="assignRoute();">Назначить</button>
                </div>
            </div>
            <div class="col-lg-4">
                <h2>Карта</h2>
                <div id="map"></div>
            </div>
        </div>
        <hr />
        <div class="col-lg-12">
            <h3>Сегодняшние маршруты</h3>
            <div id="today-routes"></div>
        </div>
    </div>
</div>

<!-- Runner select Modal -->
<div class="modal fade" id="runner-select-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <input type="text" id="runner-search" class="form-control" placeholder="Начните вводить номер телефона или имя курьера"/>
                <hr />
                <ul id="runners-list" class="list-group"></ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Ок</button>
            </div>
        </div>
    </div>
</div>

<!-- Route delete Modal -->
<div class="modal fade" id="route-delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Удалить маршрут?</h4>
            </div>
            <input type="hidden" id="delete-route-id" />
            <div class="modal-footer">
                <button type="button" class="btn btn-warning pull-left" onclick="deleteRoute();">Удалить</button>
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Оставить</button>
            </div>
        </div>
    </div>
</div>
<!-- Route delete Modal -->
<div class="modal fade" id="route-construction-error-message-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Невозможно проложить маршрут.</h4>
            </div>
            <div class="modal-body">
                <p>
                    Попробуйте уточнить адрес, корпус дома или подъезд.<br />
                    Например вместо &quot;Бисертская 16к2&quot; написать &quot;Бисертская 16к2 подъезд 1&quot;
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Понятно</button>
            </div>
        </div>
    </div>
</div>