<?php

use yii\grid\GridView;
use app\models\Route;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $route app\models\Route */
/* @var $dataProvider yii\data\ActiveDataProvider */

try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class'=>'table table-striped'],
        'columns' => [
            [
                'header' => "Курьер",
                'format' => "raw",
                'value' => function(Route $route) {
                    $time = \Yii::$app->formatter->asTime($route->created, "HH:mm");
                    if (empty($route->runner)) {
                        return "
                                <p style='text - align: right; padding: 0;'><sup>$time</sup></p>
                                <span style='color:red'>Курьер удалён</span>
                            ";
                    }
                    $runner = $route->runner;
                    $phone = isset($runner->phone)?"<br /><i>{$runner->phone}</i>":'';
                    return "
                        <p><sup>{$time}</sup></p>
                        {$runner->last_name} {$runner->first_name}{$phone}
                    ";
                }
            ],
            [
                'header' => "Точки",
                'format' => "raw",
                'value' => function(Route $route) {
                    $result = '';
                    foreach ($route->points as $point) {
                        $result .= "<li>{$point->address}</li>";
                    }
                    return "<ol>$result</ol>";
                }
            ],
            [
                'header' => "Дистанция(км)",
                'format' => "raw",
                'value' => function(Route $route) {
                    return \Yii::$app->formatter->asDecimal(($route->distance) / 1000, 3);
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['width' => '2'],
                'contentOptions' => ['style'=>'vertical-align: middle;'],
                'visibleButtons' => [
                    'view' => false,
                    'update' => false,
                    'delete' => true,

                ],
                'buttons' => [
                    'delete' => function ($url, Route $route) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-remove-sign"></span>',
                            null, ['title' => "Удалить маршрут", 'data-route-id'=>"{$route->id}", "data-toggle"=>"modal", 'data-target'=>"#route-delete-modal"]);
                    },
                ],
            ],
        ],
    ]);
} catch (\Exception $e) {
    echo $e->getMessage();
}
?>

