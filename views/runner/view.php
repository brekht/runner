<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Runner */

$this->title = $model->first_name . " ". $model->last_name;
$this->params['breadcrumbs'][] = ['label' => 'Курьеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="runner-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Действительно удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'last_name',
            'first_name',
            'second_name',
            'phone',
            'description'
        ],
    ]) ?>

</div>
