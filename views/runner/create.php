<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Runner */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Курьеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="runner-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
