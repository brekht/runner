<?php

namespace app\controllers;

use app\models\Kitchen;
use app\models\MainReport;
use app\models\Route;
use app\models\Runner;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;

class ReportController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'denyCallback' => function(){
                    if (\Yii::$app->user->isGuest) {
                        $this->redirect(['site/login']);
                    } else {
                        throw new ForbiddenHttpException();
                    }
                },

                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function toWinArr($arr)
    {
        $result = [];
        foreach ($arr as $key=>$el) {
            $result[$key] = iconv('UTF-8', 'windows-1251', $el);
        }
        return $result;
    }

    public function actionIndex()
    {
        $report = new MainReport();

        if (\Yii::$app->request->isGet || \Yii::$app->request->isPjax ) {
            $report->load(\Yii::$app->request->get());

            if ($report->action == 'download') {
                $rows = $report->sumQuery()->all();
                $file_handle = fopen("php://memory", "rw");

                fputcsv($file_handle, $this->toWinArr(["Период: ", "с {$report->start_date} по {$report->end_date}"]), ";");
                $start_point = Kitchen::getAddresses()[$report->start_point];
                if ($report->start_point == 2) {
                    fputcsv($file_handle, $this->toWinArr(["С учётом бонуса по начальной точке: ", "$report->bonus метров за маршрут"]), ";");
                }
                /*
                if ($report->start_point == 1) {
                    $start_point = Kitchen::ADDRESS_1;
                } else {
                    $start_point = Kitchen::ADDRESS_2;
                    fputcsv($file_handle, $this->toWinArr(["С учётом бонуса по начальной точке: ", "$report->bonus метров за маршрут"]), ";");
                }
                */
                fputcsv($file_handle, $this->toWinArr(["Начальная точка: ", $start_point]), ";");
                fputcsv($file_handle, []);
                fputcsv($file_handle, $this->toWinArr(["Курьер", "Дистанция(км)"]), ";");
                foreach ($rows as $row) {
                    if ($runner = Runner::findOne($row['runner_id'])) {
                        $name = $runner->last_name . " " . $runner->first_name;
                    } else {
                        $name = "Курьер удалён id:{$row['runner_id']}";
                    }
                    $bonus_distance = $report->bonus ? $row['bonus'] : 0;
                    $reportRow = [
                        $name,
                        \Yii::$app->formatter->asDecimal(($row['distance'] + $bonus_distance) / 1000, 3)
                    ];
                    fputcsv($file_handle, $this->toWinArr($reportRow), ";");
                }

                return \Yii::$app->response->sendStreamAsFile($file_handle, "report.csv", ["mimeType" => "text/csv; charset=windows-1251"]);
            }
        }
        return $this->render('index', ['report' => $report]);
    }
}