<?php

namespace app\controllers;


use app\models\Route;
use app\models\RoutePoint;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

class RouteController extends Controller
{
    public function actionAssign()
    {
        try {
            $route = new Route();
            $data = \yii::$app->request->post();

            $route->author_id = \Yii::$app->user->id;
            $route->runner_id = $data['runner_id'];
            $route->distance = $data['distance'];

            $points = [];
            foreach ($data['points'] as $address) {
                $point = new RoutePoint();
                $point->address = $address;
                $points[] = $point;
            }

            $route->populateRelation('points', $points);

            $route->save();
            foreach ($route->points as $point) {
                $point->route_id = $route->id;
                $point->save();
            }
        } catch (\Exception $e) {
            return $e->getMessage() . "\r\n" . $e->getTraceAsString();
        }
        return "Маршрут добавлен";
    }

    public function actionToday()
    {
        $today = (new \DateTime())->format("Y-m-d");
        $query = Route::find()
            ->where(['author_id' => \Yii::$app->user->id])
            ->andWhere([">=", "created", $today])
            ->orderBy(["id" => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        return $this->renderAjax("today",['dataProvider' => $dataProvider]);
    }

    public function actionDelete()
    {
        try {
            if (\Yii::$app->request->isAjax && \Yii::$app->request->isPost && \Yii::$app->request->post("route_id")) {
                $route = Route::findOne(\Yii::$app->request->post("route_id"));
                if ($route) {
                    $route->delete();
                }
            }
        } catch (\Exception $e) {
            return $this->asJson($e->getMessage());
        }
        return $this->asJson("Маршрут удалён");
    }
}