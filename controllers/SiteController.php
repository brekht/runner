<?php

namespace app\controllers;

use app\models\Route;
use app\models\Runner;
use app\models\RunnerSearch;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

use yii\web\ForbiddenHttpException;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login', 'logout', 'index'],
                'denyCallback' => function(){
                    if (\Yii::$app->user->isGuest) {
                        $this->redirect(['login']);
                    } else {
                        throw new ForbiddenHttpException();
                    }
                },
                'rules' => [
                    [
                        'actions' => ['login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index',[]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionFindRunner($search)
    {
        $query = Runner::find();
        if ($search != '') {
            $slugs = explode(" ", $search);
            foreach ($slugs as $slug) {
                $query
                    ->orFilterWhere(['like', 'LOWER(first_name)', mb_strtolower($slug)])
                    ->orFilterWhere(['like', 'LOWER(second_name)', mb_strtolower($slug)])
                    ->orFilterWhere(['like', 'LOWER(last_name)', mb_strtolower($slug)])
                    ->orFilterWhere(['like', 'LOWER(phone)', mb_strtolower($slug)])
                    ->orFilterWhere(['like', 'LOWER(description)', mb_strtolower($slug)]);
            }

        }
        $runners = $query->all();

        $runners_items = '';
        foreach ($runners as $runner) {
            $runners_items .=
                "<li class=\"list-group-item\" onclick='selectRunner(this)' data-runner-id='" . $runner->id . "'>" .
                $runner->last_name . " " . $runner->first_name . " " . $runner->second_name .
                "</li>";
        }

        return $runners_items;
    }

    /**
     * @param integer $id ID курьера
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionGetRunnerRoutes($id)
    {
        $today = (new DateTime())->format("Y-m-d");
        $routes = Route::find()->where(["id"=>$id, "start_dt" > $today])->all();
        return $routes;
    }
}
