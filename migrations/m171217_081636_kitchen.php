<?php

use app\models\Kitchen;
use yii\db\Migration;

/**
 * Class m171217_081636_kitchen
 */
class m171217_081636_kitchen extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(Kitchen::tableName(),[
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
            'address' => $this->string(255)->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(Kitchen::tableName());
    }
}
