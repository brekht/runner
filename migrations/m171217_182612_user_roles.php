<?php

use yii\db\Migration;
use app\models\User;

/**
 * Поддержка ролей
 */
class m171217_182612_user_roles extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(User::tablename(), "role", $this->string()->notNull()->defaultValue("manager"));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(User::tablename(), "role");
    }
}
