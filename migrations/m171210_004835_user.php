<?php

use yii\db\Migration;
use app\models\User;

/**
 * Class m171210_004835_user
 */
class m171210_004835_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(User::tablename(),[
            'id' => $this->primaryKey(),
            'username' => $this->string(31)->notNull(),
            'password' => $this->string(64)->notNull(),
            'email' => $this->string(64)->null()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(User::tablename());
    }
}
