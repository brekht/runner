<?php

use yii\db\Migration;
use app\models\Runner;

/**
 * Class m171210_024135_runner
 */
class m171210_024135_runner extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(Runner::tableName(), [
            'id' => $this->primaryKey(),
            'first_name' => $this->string(31),
            'second_name' => $this->string(31),
            'last_name' => $this->string(31),
            'phone' => $this->string(15)->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(Runner::tableName());
    }

}
