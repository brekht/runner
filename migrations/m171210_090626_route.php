<?php

use yii\db\Migration;

use app\models\Route;
use app\models\RoutePoint;
use app\models\User;

/**
 * Class m171210_090626_route
 */
class m171210_090626_route extends Migration
{

    const FK_ROUTE_AUTHOR = "fk_route_author";
    const FK_POINT_ROUTE = "fk_point_route";

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(Route::tableName(), [
            'id' => $this->primaryKey(),
            'created' => $this->dateTime()->notNull()->defaultExpression("current_timestamp"),
            'author_id' => $this->integer()->notNull(),
            'runner_id' => $this->integer()->notNull(),
            'distance' => $this->float(2)->notNull(),
            'start_dt' => $this->dateTime(),
            'finish_dt' => $this->dateTime()
        ]);
        //$this->addForeignKey(self::FK_ROUTE_AUTHOR, Route::tableName(), ["author_id"], User::tablename(), ['id']);

        $this->createTable(RoutePoint::tableName(), [
            'id' => $this->primaryKey(),
            'route_id' => $this->integer()->notNull(),
            'address' => $this->text()
        ]);
        //$this->addForeignKey(self::FK_POINT_ROUTE, RoutePoint::tableName(), ["route_id"], Route::tablename(), ['id']);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        //$this->dropForeignKey(self::FK_ROUTE_AUTHOR, Route::tableName());
        //$this->dropForeignKey(self::FK_POINT_ROUTE, RoutePoint::tableName());
        $this->dropTable(RoutePoint::tableName());
        $this->dropTable(Route::tableName());
    }

}
