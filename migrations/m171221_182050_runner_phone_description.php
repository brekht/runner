<?php

use app\models\Runner;
use yii\db\Migration;

/**
 * Drop phone field
 * Add description field
 */
class m171221_182050_runner_phone_description extends Migration
{
    private $tableName = '';
    private $temporaryTableName = '';

    public function init()
    {
        $this->tableName = Runner::tableName();
        $this->temporaryTableName = $this->tableName."_tmp";
    }

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        if (YII_ENV_DEV) {
            //create new table
            $this->createTable($this->temporaryTableName, [
                'id' => $this->primaryKey(),
                'first_name' => $this->string(31)->notNull(),
                'second_name' => $this->string(31),
                'last_name' => $this->string(31)->notNull(),
                'phone' => $this->string(15),
                'description' => $this->text()
            ]);
            //copy data
            $sql_copy = "
                INSERT INTO {$this->temporaryTableName} (first_name, second_name, last_name, phone)
                  SELECT first_name, second_name, last_name, phone FROM {$this->tableName}";
            $this->execute($sql_copy, []);
            //drop old table
            $this->dropTable($this->tableName);
            //rename new table to old
            $this->renameTable($this->temporaryTableName, $this->tableName);
        } else {
            $this->alterColumn($this->tableName, 'first_name', $this->string(31)->notNull());
            $this->alterColumn($this->tableName, 'last_name', $this->string(31)->notNull());
            $this->addColumn($this->tableName, "description", $this->text());
            $this->alterColumn($this->tableName, "phone", $this->string(15)->null());
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        if (YII_ENV_DEV) {
            $this->createTable($this->temporaryTableName, [
                'id' => $this->primaryKey(),
                'first_name' => $this->string(31),
                'second_name' => $this->string(31),
                'last_name' => $this->string(31),
                'phone' => $this->string(15)->notNull()
            ]);
            $sql_copy = "
                INSERT INTO {$this->temporaryTableName} (first_name, second_name, last_name, phone)
                  SELECT first_name, second_name, last_name, phone FROM {$this->tableName}";
            $this->execute($sql_copy, []);
            $this->dropTable($this->tableName);
            $this->renameTable($this->temporaryTableName, $this->tableName);
        } else {
            $this->dropColumn($this->tableName, "description");
            $this->addColumn($this->tableName, "phone", $this->string(15)->notNull());
            $this->alterColumn($this->tableName, 'first_name', $this->string(31)->null());
            $this->alterColumn($this->tableName, 'last_name', $this->string(31)->null());
        }
    }
}
