<?php

namespace app\models;


use yii\db\ActiveRecord;

/**
 * Курьер
 * @package app\models
 *
 * @property integer $id
 * @property string $first_name
 * @property string $second_name
 * @property string $last_name
 * @property string $phone
 */
class Runner extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return "runner";
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['first_name', 'required', 'message' => 'Нужно указать имя'],
            ['last_name', 'required', 'message' => 'Нужно указать фамилию'],
            [
                [
                    'second_name',
                    'phone',
                    'description'
                ],
                'safe'],
            [['phone'], 'string', 'max' => 15, 'message'=>"Номер не может быть более 15 знаков"],
        ];
    }

    public function attributeLabels()
    {
        return [
            'first_name' => "Имя",
            'second_name' => "Отчество",
            'last_name' => "Фамилия",
            'phone' => "Телефон",
            'description' => 'Дополнительная информация'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoutes()
    {
        return $this->hasMany(Route::className(), ['runner_id' => 'id']);
    }
}