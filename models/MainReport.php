<?php

namespace app\models;


use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\db\Query;
use yii\db\QueryBuilder;

/**
 * Отчёт за период
 * @property \DateTime $start_date
 * @property \DateTime $end_date
 * @property integer $start_point
 *
 * @property integer $bonus
 */
class MainReport extends Model
{
    /**
     * Бонусные километры
     * Начисляются на каждый маршрут из Kitchen::ADDRESS_2
     */
    const BONUS = 1500;

    public $start_date;
    public $end_date;
    public $start_point;

    public $action;

    public function rules()
    {
        return [
            [['start_date', 'end_date', 'start_point', 'action'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'start_date' => 'Начало периода',
            'end_date' => 'Окончание периода',
            'start_point' => 'Начальный адрес'
        ];
    }
    /*
    public function formName()
    {
        return '';
    }
    */

    public function getBonus()
    {
        return ($this->start_point == 2) ? self::BONUS : false;
    }

    /**
     * @return Query
     * @throws \yii\base\InvalidConfigException
     */
    public function query()
    {
        $kitchen = Kitchen::getAddresses()[$this->start_point];
        /*
        switch ($this->start_point) {
            case 1:
                $kitchen = Kitchen::ADDRESS_1;
                break;
            case 2:
                $kitchen = Kitchen::ADDRESS_2;
                break;
            default:
                $kitchen = Kitchen::ADDRESS_2;
        }
        */
        $q = (new Query)
            ->from("route")
            ->select([
                'runner_id'=>'route.runner_id',
                'route_id'=>'route.id',
                'created'=>'route.created',
                'route_distance'=>'route.distance',
                'start_point_id' => 'MIN(route_point.id)',
            ])

            ->innerJoin('route_point', "route_point.route_id = route.id")

            ->andFilterWhere([">", "route.created", \Yii::$app->formatter->asDate($this->start_date, "yyyy-MM-dd HH:mm")])
            ->andFilterWhere(["<", "route.created", \Yii::$app->formatter->asDate($this->end_date, "yyyy-MM-dd HH:mm")])

            ->groupBy("route.runner_id, route.id, route.created, route.distance");
            //->having(["route_point.address"=> $kitchen]);

        $bonus_expression = new Expression($this->bonus?:0);

        return (new Query)
            ->from(['q' => $q])
            ->select([
                'runner_id'=>'q.runner_id',
                'route_id'=>'q.route_id',
                'created'=>'q.created',
                'route_distance'=>'q.route_distance',
                'route_distance_bonus'=>$bonus_expression,
                //'route_distance_with_bonus'=>'CASE `rp`.`address` WHEN "'.Kitchen::ADDRESS_2.'1'.'" THEN `q`.`route_distance` + 1500 ELSE `q`.`route_distance` END',
                'start_point_id' => 'q.start_point_id',
            ])
            ->innerJoin("route_point rp", "rp.id = q.start_point_id")
            ->where(['rp.address' => $kitchen]);
    }

    public function mainQuery()
    {
        return (new Query)
            ->from(["x" => $this->query()])
            ->leftJoin("runner", "runner.id = x.runner_id")
            ->select([
                'created'=>'x.created',
                'runner_id'=>'x.runner_id',
                'runner_first_name'=>'runner.first_name',
                'runner_last_name'=>'runner.last_name',
                'distance'=>'x.route_distance',
                'bonus'=>'x.route_distance_bonus',
                'phone'=>'runner.phone',
                'route_id'=>'x.route_id'
            ])
            ->orderBy("runner.last_name, x.created");
    }

    public function sumQuery()
    {
        return (new Query)
            ->from(["x" => $this->query()])
            ->select([
                'runner_id'=>'x.runner_id',
                'distance'=>'SUM(x.route_distance)',
                'bonus'=>'SUM(x.route_distance_bonus)'
            ])
            ->groupBy("x.runner_id")
            ->orderBy("distance");
    }

    /**
     * @return ActiveDataProvider
     * @throws \yii\base\InvalidConfigException
     */
    public function getMainDataProvider()
    {
        return new ActiveDataProvider([
            'query' => $this->mainQuery(),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
    }

    /**
     * @return ActiveDataProvider
     * @throws \yii\base\InvalidConfigException
     */
    public function getSumDataProvider()
    {
        $query = Route::find()
            ->andFilterWhere([">", "created", \Yii::$app->formatter->asDate($this->start_date, "yyyy-MM-dd HH:mm")])
            ->andFilterWhere(["<", "created", \Yii::$app->formatter->asDate($this->end_date, "yyyy-MM-dd HH:mm")]);
        return new ActiveDataProvider([
            'query' => $this->sumQuery(),
            'pagination' => false
        ]);
    }
}