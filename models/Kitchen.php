<?php
/**
 * Created by PhpStorm.
 * User: brekht
 * Date: 17.12.17
 * Time: 13:17
 */

namespace app\models;


use yii\db\ActiveRecord;

/**
 * Кухни
 *
 * @property integer $id суррогатный первичный ключ
 * @property string $title название
 * @property string $address адрес
 *
 */
class Kitchen extends ActiveRecord
{
    const ADDRESS_PREFIX = "Россия, Свердловская область, Екатеринбург, ";

    const ADDRESS_1 = "улица Титова, 1";
    const ADDRESS_2 = "проспект Космонавтов, 11к2";
    const ADDRESS_3 = "улица 8 Марта, 46";

    const ADDRESS_4 = "улица Победы, 14А";
    const ADDRESS_5 = "улица Вильгельма де Геннина, 42";

    public static function tableName()
    {
        return "kitchen";
    }

    public static function getAddresses($withPrefix = true)
    {
        $prefix = $withPrefix ? self::ADDRESS_PREFIX : '';
        return [
            1 => $prefix . self::ADDRESS_1,
            2 => $prefix . self::ADDRESS_2,
            3 => $prefix . self::ADDRESS_3,
            4 => $prefix . self::ADDRESS_4,
            5 => $prefix . self::ADDRESS_5,
        ];
    }
}