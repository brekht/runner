<?php

namespace app\models;

use yii\db\ActiveRecord;


/**
 * Точка маршрута
 * @package app\models
 *
 * @property integer $id
 * @property integer $route_id
 * @property string $address
 *
 * @property Route $route
 */
class RoutePoint extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return "route_point";
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoute()
    {
        return $this->hasOne(Route::className(), ['id' => 'route_id']);
    }
}