<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\web\IdentityInterface;


/**
 * Class User
 * @package app\models
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $role
 *
 */
class User extends ActiveRecord implements IdentityInterface
{
    const ROLE_ADMIN = "administrator";
    const ROLE_MANAGER = "manager";

    public static function getRoleTitle($role)
    {
        switch ($role) {
            case self::ROLE_ADMIN:
                return "Администратор";
            case self::ROLE_MANAGER:
                return "Менеджер";
        }
    }

    /**
     * Имя таблицы пользователей
     * @return string
     */
    public static function tablename()
    {
        return "user";
    }

    public function rules()
    {
        return [
            [['username', 'password', 'role', 'email'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => "Логин",
            'role' => "Роль",
            'email' => "Почта"
        ];
    }

    /**
     * @param array $data
     * @param null $formName
     * @return bool
     * @throws \yii\base\Exception
     */
    public function load($data, $formName = null)
    {
        $parent_load = parent::load($data, $formName);
        if ($this->isNewRecord){
            $this->setPassword($this->password);
        }
        return $parent_load;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return self::findOne(['username' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $password
     * @throws \yii\base\Exception
     */
    public function setPassword($password)
    {
        $this->password = \Yii::$app->getSecurity()->generatePasswordHash($password);
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return \Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {

    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {

    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {

    }
}
