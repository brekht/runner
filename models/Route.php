<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Маршрут
 * @package app\models
 *
 * @property integer $id
 * @property integer $runner_id
 * @property integer $author_id
 * @property \DateTime $start_dt
 * @property \DateTime $finish_dt
 * @property RoutePoint[] $points
 * @property float $distance
 */
class Route extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return "route";
    }

    public function attributeLabels()
    {
        return [
            'distance' => "Длина маршрута"
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoints()
    {
        return $this->hasMany(RoutePoint::className(), ['route_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRunner()
    {
        return $this->hasOne(Runner::className(), ['id' => 'runner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }
}