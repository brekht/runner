ymaps.ready(init);
var myMap;
var routePoints = [];
var routeLength = 0;
var runner = -1;
var addressPrefix = 'Россия, Свердловская область, Екатеринбург, ';
/*
var kitchen = [
    "Россия, Свердловская область, Екатеринбург, " + "проспект Космонавтов, 11к2",
    "Россия, Свердловская область, Екатеринбург, " + "улица 8 Марта, 46",
    "Россия, Свердловская область, Екатеринбург, " + "улица Титова, 1"
];
*/
var approvedAddress = '-1';
//var currentKitchen = 0;

function clearSpace() {
    routePoints = [];
    routeLength = 0;
    runner = -1;
    $("#runner").data("runnerId", runner);
    $("#runner").text('Выбрать курьера');
    $("#route ol").html('');
    $("#route-length .badge").text('');
    $("#route-length").slideUp();
    refreshAssignRouteButton();
}

function init(){
    myMap = new ymaps.Map("map", {
        center: [56.83, 60.60],
        zoom: 11,
        controls: [],
        //bounds: [[56.760570, 60.440948], [56.899852, 60.711487]]
    },{
        suppressMapOpenBlock: true,
    });

    //var point_search = new ymaps.SuggestView('point-search',{boundedBy: myMap.getBounds(), strictBounds: true, routingMode: "auto"});
    //var point_search = new ymaps.SuggestView('point-search',{boundedBy: [[56.760570, 60.440948], [56.899852, 60.711487]], strictBounds: true, routingMode: "auto"});
    var point_search = new ymaps.SuggestView('point-search',{
        boundedBy: [[56.703133, 60.277262], [57.021201, 60.934350]],
        strictBounds: true, 
        routingMode: "auto",
        provider: {
            suggest: function (request, options) {
                return ymaps.suggest(
                    "Россия, Свердловская область, " + request,
                    {boundedBy: [[56.703133, 60.277262], [57.021201, 60.934350]]}
                );
            }
        }
    });
    point_search.events.add(["select"], function(event){
        var point = event.get("item");
        approvedAddress = point.value;
        $("#point-search").change();
        //addPoint(point.value)
        //$("#point-search").val('');
        //calculate();
    });
}
function checkApproved() {
    var checkedAddress = $("#point-search").val();
    $("#add-that-point").prop("disabled", (approvedAddress !== checkedAddress));
}
function addThatPoint(el) {
    value = $(el).parent().siblings("input").val();
    addPoint(value);
    $("#point-search").val('');
}
function addPoint(point) {
    routePoints.push(point);
    refreshPointsHTML();
}
function removePoint(el) {
    var index = $(el).parent().data("index");
    routePoints.splice(index,1);
    refreshPointsHTML();
}
function refreshPointsHTML() {
    $("#route ol").html('');
    $.each(routePoints, function(index, value){
        $("#route ol").append(
            "<li class='selected_point list-group-item' data-index=\"" + index + "\">" +
            "<button type=\"button\" class=\"btn btn-default close\" onclick=\"removePoint(this);\"><span aria-hidden=\"true\">&times;</span></button>" +
            value +
            "</li>"
        );
    });
}

function calculate() {
    var preparedPoints = [];
    $.each(routePoints, function (index, value) {
        var point;
        if (index !== 0 && index < routePoints.length - 1) {
            point = {
                //type: 'wayPoint',
                type: 'viaPoint',
                point: value
            };
        } else {
            point = {
                type: 'wayPoint',
                point: value
            };
        }
        preparedPoints.push(point);
    });
    ymaps.route(preparedPoints, {mapStateAutoApply: true, routingMode: "auto", avoidTrafficJams: true}).then(function(route){
        console.log(route);
        myMap.geoObjects.removeAll();
        myMap.geoObjects.add(route);
        routeLength = route.getLength();
        $("#route-length .badge").text(routeLength + " метров");
        $("#route-length").slideDown();
        refreshAssignRouteButton();
    }).catch(function(err){
        $("#route-construction-error-message-modal").modal('show');
    });
}

function selectRunner(el) {
    $("#runners-list li").removeClass("active");
    $(el).addClass("active");
    $("#runner").text($(el).text());
    $("#runner").data("runnerId", $(el).data("runnerId"));
    runner = $(el).data("runnerId");
    refreshAssignRouteButton();
}

function addKitchenToRoute() {
    //var point = kitchen[currentKitchen];
    var point = addressPrefix + $("[name='current-kitchen'] option:selected").text();
    addPoint(point);
}

$(document).ready(function(){
    $('#runner-select-modal').on('show.bs.modal', function (event) {
        runnerSearch('');
    });

    $("#runner-search").on("change input", function(){
        runnerSearch($(this).val());
    });
    showUserTodayRoutes();

    $("#point-search").on('change input', function(){checkApproved();});

    $('#route-delete-modal').on('show.bs.modal', function (event) {
        var routeId = $(event.relatedTarget).data('routeId') // Button that triggered the modal
        $(this).find('#delete-route-id').val(routeId);
    });
});

function runnerSearch(search) {
    $.ajax("/site/find-runner", {
        data: {search:search},
        success: function(data) {
            $("#runners-list").html(data);
        },
        error: function () {
            alert("Error");
        }
    });
}

function canAssignRoute() {
    return (routeLength > 0 && runner > -1);
}

function refreshAssignRouteButton() {
    if (canAssignRoute()) {
        $("#assign-route").attr('disabled', false);
    } else {
        $("#assign-route").attr('disabled', true);
    }
}

function assignRoute() {
    $("#assign-route").attr('disabled', true);
    $.ajax("/route/assign", {
        method: 'post',
        data: {
            runner_id: runner,
            points: routePoints,
            distance: routeLength
        },
        success: function (data) {
            clearSpace();
            showUserTodayRoutes();
        },
        error: function () {
            alert("Ошибка");
        }
    });
}

function showUserTodayRoutes() {
    $.ajax("/route/today", {
        method: "post",
        data: {},
        success: function(data) {
            $("#today-routes").html(data);
        }
    });
}

function deleteRoute() {
    var routeId = $('#route-delete-modal #delete-route-id').val();
    $.ajax("/route/delete", {
        method: 'post',
        data: {
            route_id: routeId
        },
        success: function (data) {
            showUserTodayRoutes();
            $('#route-delete-modal').modal('hide');
        }
    });
}