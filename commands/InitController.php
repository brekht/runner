<?php

namespace app\commands;

use app\models\User;
use yii\console\Controller;
use yii\helpers\Console;

class InitController extends Controller
{
    /**
     * Создаёт учётку админа
     *
     * @param $name
     * @param $password
     * @param null $email
     * @throws \yii\base\Exception
     */
    public function actionSetAdmin($name, $password, $email = null)
    {
        if ($name && $password) {
            $admin = new User();
            $admin->username = $name;
            $admin->setPassword($password);
            $admin->email = $email;
            $admin->role = User::ROLE_ADMIN;
            $admin->save();

            $this->stdout("Создана учётная запись администратора ");
            $this->stdout("\"$name\"", Console::FG_YELLOW);
            $this->stdout(PHP_EOL);
        }
    }
}
